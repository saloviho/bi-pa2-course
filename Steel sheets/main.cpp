#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <climits>
#include <cfloat>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <vector>
#include <set>
#include <list>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <queue>
#include <stack>
#include <deque>
#include <memory>
#include <functional>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include "progtest_solver.h"
#include "sample_tester.h"
using namespace std;
#endif /* __PROGTEST__ */ 

mutex mtx;
condition_variable   cv_prod;
condition_variable   cv_empty;
int runningProd = 0;
int runningCons = 0;

struct item_t
{
	AOrderList list;
	ACustomer cus;
	COrder & ord;
	unsigned int * solved;
	bool end;
};

class OrderBuffer
{
private:
	deque<item_t*> buff;

public:
	OrderBuffer() { }

	void insert(item_t *item)
	{
		unique_lock<mutex> ul(mtx);
		buff.push_back(item);
		cv_empty.notify_one();
	}

	item_t * remove()
	{
		item_t *item;
		unique_lock<mutex> ul(mtx);
		cv_empty.wait(ul, [this]() { return (!buff.empty()); });
		item = buff.front();
		buff.pop_front();
		return item;
	}

	bool empty()
	{
		return buff.empty();
	}
};


bool srt(const CProd & a, const CProd & b)
{
	if (min(a.m_W, a.m_H) < min(b.m_W, b.m_H)) return false;
	else if (min(a.m_W, a.m_H) > min(b.m_W, b.m_H)) return true;
	else {
		if (max(a.m_W, a.m_H) < max(b.m_W, b.m_H)) return false;
		else if (max(a.m_W, a.m_H) > max(b.m_W, b.m_H)) return true;
		else {
			if (a.m_Cost < b.m_Cost) return true;
			if (a.m_Cost > b.m_Cost) return false;
			else return false;
		}
	}
}

bool unq(const CProd & a, const CProd & b)
{
	if ((a.m_W == b.m_W && a.m_H == b.m_H)
		|| (a.m_W == b.m_H && a.m_H == b.m_W)) return true;
	return false;
}

class PriceBuffer
{
private:
	map<int, APriceList> prices;
	map<int, set<AProducer>> producers;
	unsigned int m_pc = 0;

public:
	PriceBuffer() { }

	void inc() {
		m_pc++;
	}

	map<int, APriceList>::iterator find(int id)
	{
		return prices.find(id);
	}

	void insert(AProducer prod, APriceList list)
	{
		unique_lock<mutex> ul(mtx);

		if (full(list->m_MaterialID)) {
			cv_prod.notify_all();
			return;
		}

		auto it1 = prices.find(list->m_MaterialID);
		auto it2 = producers.find(list->m_MaterialID);
		if (it1 == prices.end()) {
			prices.insert(make_pair(list->m_MaterialID, list));
			set<AProducer> s; s.insert(prod);
			producers.insert(make_pair(list->m_MaterialID, s));
			it1 = prices.find(list->m_MaterialID);
			it2 = producers.find(list->m_MaterialID);
		}

		vector<CProd> tmp = it1->second->m_List;
		tmp.insert(tmp.end(), list->m_List.begin(), list->m_List.end());
		sort(tmp.begin(), tmp.end(), srt);

		auto u = unique(tmp.begin(), tmp.end(), unq);
		vector<CProd> lol(tmp.begin(), u);

		it1->second->m_List = lol;
		it2->second.insert(prod);

		cv_prod.notify_all();
	}

	bool full(int materialID)
	{
		auto it = producers.find(materialID);
		if (it == producers.end()) return false;
		return it->second.size() >= m_pc;
	}
};


class CWeldingCompany
{
public:
	static void SeqSolve(APriceList priceList, COrder & order);
	void Solve(item_t * item);
	void AddProducer(AProducer prod);
	void AddCustomer(ACustomer cust);
	void AddPriceList(AProducer prod, APriceList priceList);
	void Start(unsigned thrCount);
	void Stop(void);

	void Worker(void);
	void Customer(ACustomer & cus);
private:
	PriceBuffer prices;
	OrderBuffer orders;
	vector<ACustomer> c_List;
	vector<AProducer> p_List;
	vector<thread> c_Threads;
	vector<thread> w_Threads;
};

void CWeldingCompany::AddPriceList(AProducer prod, APriceList priceList)
{
	prices.insert(prod, priceList);
}

void CWeldingCompany::Worker(void)
{
	unique_lock<mutex> ul(mtx);
	while (!(orders.empty() && runningProd == 0))
	{
		ul.unlock();
		item_t * item = orders.remove();
		if (item->end) {
			delete(item);
			break;
		}

		ul.lock();
		if (!prices.full(item->list->m_MaterialID)) {
			ul.unlock();
			for (size_t i = 0; i < p_List.size(); i++) p_List[i]->SendPriceList(item->list->m_MaterialID);
		}
		else ul.unlock();

		Solve(item);
		delete(item);
		ul.lock();
	}
}

void CWeldingCompany::Customer(ACustomer & cus)
{
	while (1)
	{
		AOrderList list = cus->WaitForDemand();
		unique_lock<mutex> ul(mtx);

		if (list == NULL) {
			runningProd--;
			if (runningProd == 0)
			{
				ul.unlock();
				for (int i = 0; i < runningCons; i++)
				{
					COrder a(0, 0, 0);
					item_t * item = new item_t{ NULL, cus, a, NULL, true };
					orders.insert(item);
				}
			}
			break;
		}
		ul.unlock();
		unsigned int * solved = new unsigned int(0);
		for (size_t i = 0; i < list->m_List.size(); i++) {
			item_t * item = new item_t{ list, cus, list->m_List[i], solved, false };
			orders.insert(item);
		}
	}
}


void CWeldingCompany::Solve(item_t * item) {
	unique_lock<mutex> ul(mtx);
	cv_prod.wait(ul, [item, this]() { return prices.full(item->list->m_MaterialID); });
	APriceList tmp = prices.find(item->list->m_MaterialID)->second;
	ul.unlock();

	SeqSolve(tmp, item->ord);
	ul.lock();
	*(item->solved) = *(item->solved) + 1;
	if (*(item->solved) == item->list->m_List.size()) {
		ul.unlock();
		item->cus->Completed(item->list);
		delete(item->solved);
	}
}

void CWeldingCompany::SeqSolve(APriceList priceList, COrder & order)
{
	size_t size = max(order.m_W, order.m_H) + 1;
	double cache[750][750];
	order.m_Cost = DBL_MAX;
	vector<CProd> second = priceList->m_List;

	for (size_t i = 0; i < size; i++)
		for (size_t j = 0; j < size; j++) cache[i][j] = -1;

	cache[0][0] = 0;

	for (size_t i = 0; i < second.size(); i++) {
		int m = max(second[i].m_W, second[i].m_H);
		int k = min(second[i].m_W, second[i].m_H);

		if (cache[m][k] == -1) cache[m][k] = second[i].m_Cost;
		else {
			if (cache[m][k] > second[i].m_Cost) cache[m][k] = second[i].m_Cost;
		}
		cache[k][m] = cache[m][k];
	}


	for (size_t i = 1; i < size; i++) {
		for (size_t j = 1; j <= i; j++) {
			size_t max_i_j = max(i, j);
			for (size_t k = 0; k <= max_i_j; k++) {
				double res1 = -1;
				double res2 = -1;
				double res = -1;

				if (k <= j) if (cache[i][k] != -1 && cache[i][j - k] != -1) res1 = cache[i][k] + cache[i][j - k] + i * order.m_WeldingStrength;
				if (k <= i) if (cache[k][j] != -1 && cache[i - k][j] != -1) res2 = cache[k][j] + cache[i - k][j] + j * order.m_WeldingStrength;

				if (res1 != -1 && res2 == -1) res = res1;
				else if (res1 == -1 && res2 != -1) res = res2;
				else if (res1 != -1 && res2 != -1) res = min(res1, res2);


				if (res != -1) {
					if (cache[i][j] == -1) cache[i][j] = res;
					else if (cache[i][j] > res) cache[i][j] = res;
				}

				cache[j][i] = cache[i][j];
			}
		}
	}

	if (cache[order.m_W][order.m_H] != -1) order.m_Cost = cache[order.m_W][order.m_H];
}

void CWeldingCompany::AddProducer(AProducer prod)
{
	p_List.push_back(prod);
	prices.inc();
}

void CWeldingCompany::AddCustomer(ACustomer cust)
{
	c_List.push_back(cust);
}

void CWeldingCompany::Start(unsigned thrCount)
{
	runningCons = thrCount;
	runningProd = c_List.size();
	for (size_t i = 0; i < c_List.size(); i++) c_Threads.push_back(thread(&CWeldingCompany::Customer, this, ref(c_List[i])));
	for (size_t i = 0; i < thrCount; i++) w_Threads.push_back(thread(&CWeldingCompany::Worker, this));
}

void CWeldingCompany::Stop(void)
{
	for (size_t i = 0; i < c_Threads.size(); i++) c_Threads[i].join();
	for (size_t i = 0; i < w_Threads.size(); i++) w_Threads[i].join();
}

//-------------------------------------------------------------------------------------------------
#ifndef __PROGTEST__
int                main(void)
{
	using namespace std::placeholders;
	CWeldingCompany  test;

	AProducer p1 = make_shared<CProducerSync>(bind(&CWeldingCompany::AddPriceList, &test, _1, _2));
	AProducerAsync p2 = make_shared<CProducerAsync>(bind(&CWeldingCompany::AddPriceList, &test, _1, _2));
	test.AddProducer(p1);
	test.AddProducer(p2);
	test.AddCustomer(make_shared<CCustomerTest>(2));
	p2->Start();
	test.Start(5);
	test.Stop();
	p2->Stop();
	return 0;
}
#endif /* __PROGTEST__ */ 
