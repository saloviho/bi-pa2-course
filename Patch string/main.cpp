﻿
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <memory>
#include <string>
using namespace std;

class InvalidIndexException
{
};

#endif /* __PROGTEST__ */

class CString
{
public:
	CString(const char * in)
	{
		m_Length = strlen(in);
		m_Size = m_Length + 1;
		m_Text = new char[m_Size];
		for (size_t i = 0; i < m_Size; i++) m_Text[i] = in[i];
	}

	CString(const CString & in)
	{
		m_Length = in.m_Length;
		m_Size = in.m_Size;
		m_Text = new char[m_Size];
		for (size_t i = 0; i < m_Size; i++) m_Text[i] = in.m_Text[i];
	}

	~CString(void)
	{
		delete[] m_Text;
	}

	CString & operator = (const CString & in)
	{
		if (this == &in) return *this;

		delete[] m_Text;
		m_Length = in.m_Length;
		m_Size = in.m_Size;
		m_Text = new char[m_Size];
		for (size_t i = 0; i < m_Size; i++) m_Text[i] = in.m_Text[i];

		return *this;
	}
	size_t m_Length;
	size_t m_Size;
	char * m_Text;
private:

};

class CNode
{
public:

	CNode() {}

	CNode(size_t offset, size_t length, const char * text) : m_Offset(offset), m_Length(length)
	{
		m_Ptr = make_shared<CString>(text);
	}

	size_t m_Offset;
	size_t m_Length;
	shared_ptr<CString> m_Ptr;
private:

};


class CPatchStr
{
public:
	CPatchStr(void)
	{
		m_Data = new CNode[10];
		m_Size = 0;
		m_Length = 0;
		m_Allocated = 10;
	}

	CPatchStr(const char * str)
	{
		m_Data = new CNode[10];
		m_Data[0] = CNode(0, strlen(str), str);
		m_Allocated = 10;
		m_Size = 1;
		m_Length = strlen(str);
	}

	CPatchStr(const CPatchStr & in)
	{
		m_Size = in.m_Size;
		m_Allocated = in.m_Allocated;
		m_Length = in.m_Length;
		m_Data = new CNode[m_Allocated];
		for (size_t i = 0; i < m_Size; i++) m_Data[i] = in.m_Data[i];
	}

	CPatchStr(const CNode & in)
	{
		m_Data = new CNode[10];
		m_Data[0] = in;
		m_Allocated = 10;
		m_Size = 1;
		m_Length = in.m_Length;
	}

	~CPatchStr(void)
	{
		delete[] m_Data;
	}

	CPatchStr & operator = (const CPatchStr & in)
	{
		if (this == &in) return *this;
		delete[] m_Data;

		m_Size = in.m_Size;
		m_Allocated = in.m_Allocated;
		m_Length = in.m_Length;
		m_Data = new CNode[m_Allocated];
		for (size_t i = 0; i < m_Size; i++) m_Data[i] = in.m_Data[i];

		return *this;
	}
	CPatchStr SubStr(size_t from, size_t len) const
	{
		CPatchStr result;
		if (len == 0) return result;

		size_t currentPos = 0, found = 0, remind = len, totallen = 0;
		size_t i = 0;
		for (; i < m_Size; i++)
		{
			if (from < currentPos + m_Data[i].m_Length) {
				CNode tmp;
				tmp.m_Ptr = m_Data[i].m_Ptr;
				if (found == 0)
				{
					tmp.m_Offset = from - currentPos + m_Data[i].m_Offset;
					found = 1;
				}
				else tmp.m_Offset = m_Data[i].m_Offset;

				if (from + len < currentPos + m_Data[i].m_Length)
				{
					tmp.m_Length = remind;
					totallen = totallen + tmp.m_Length;
					result.Append(CPatchStr(tmp));
					break;
				}
				else {
					tmp.m_Length = m_Data[i].m_Length - tmp.m_Offset + m_Data[i].m_Offset;
					remind = remind - tmp.m_Length;
					totallen = totallen + tmp.m_Length;
				}
				result.Append(CPatchStr(tmp));
			}
			currentPos = currentPos + m_Data[i].m_Length;
		}

		if (i == m_Size && from + len > currentPos) throw InvalidIndexException();
		result.m_Length = totallen;
		return result;
	}

	CPatchStr & Append(const CPatchStr & src)
	{
		CPatchStr tmp = src;
		for (size_t i = 0; i < tmp.m_Size; i++) {
			if (m_Size >= m_Allocated) Resize();
			m_Data[m_Size] = tmp.m_Data[i];
			m_Size++;
		}

		m_Length = m_Length + src.m_Length;

		return *this;
	}

	CPatchStr & Insert(size_t pos, const CPatchStr & src) {
		CPatchStr a;
		CPatchStr b = this->SubStr(0, pos);
		CPatchStr c = this->SubStr(pos, m_Length - pos);

		a.Append(b);
		a.Append(src);
		a.Append(c);

		*this = a;
		return *this;
	}

	CPatchStr & Delete(size_t from, size_t len)
	{
		if (len == 0) return *this;
		if (from + len > m_Length) throw InvalidIndexException();

		CPatchStr a;
		CPatchStr b = this->SubStr(0, from);
		CPatchStr c = this->SubStr(from + len, m_Length - from - len);
		a.Append(b);
		a.Append(c);
		*this = a;
		return *this;
	}

	char * ToStr(void) const
	{
		char * result = new char[m_Length + 1];
		int index = 0;

		for (size_t i = 0; i < m_Size; i++)
		{
			for (size_t j = m_Data[i].m_Offset; j < m_Data[i].m_Offset + m_Data[i].m_Length; j++)
			{
				result[index] = m_Data[i].m_Ptr->m_Text[j];
				index++;
			}
		}

		result[index] = '\0';
		return result;
	}

private:
	void Resize()
	{
		m_Allocated = m_Allocated * 2;
		CNode * tmp = new CNode[m_Allocated];
		for (size_t i = 0; i < m_Size; i++) tmp[i] = m_Data[i];
		delete[] m_Data;
		m_Data = tmp;
	}

	CNode * m_Data;
	size_t m_Size;
	size_t m_Length;
	size_t m_Allocated;
};

#ifndef __PROGTEST__
bool stringMatch(char       * str,
	const char * expected)
{
	bool res = strcmp(str, expected) == 0;
	delete[] str;
	return res;
}

int main(void)
{
	char tmpStr[100];

	{
		CPatchStr a;
		assert(stringMatch(a.ToStr(), ""));
		CPatchStr b("");
		b.Delete(0, 0);
		try
		{
			b.Delete(0, 1);
			assert("Exception not thrown" == NULL);
		}
		catch (InvalidIndexException & e)
		{
		}

		try
		{
			b.SubStr(0, 1);
			assert("Exception not thrown" == NULL);
		}
		catch (InvalidIndexException & e)
		{
		}

		a.Append(b);
		a.SubStr(0, 0);
		a = b;
		CPatchStr c(a);
		assert(stringMatch(a.ToStr(), ""));
	}

	CPatchStr a("test");
	assert(stringMatch(a.ToStr(), "test"));
	strncpy(tmpStr, " da", sizeof(tmpStr));
	a.Append(tmpStr);
	assert(stringMatch(a.ToStr(), "test da"));
	strncpy(tmpStr, "ta", sizeof(tmpStr));
	a.Append(tmpStr);
	assert(stringMatch(a.ToStr(), "test data"));
	strncpy(tmpStr, "foo text", sizeof(tmpStr));
	CPatchStr b(tmpStr);
	assert(stringMatch(b.ToStr(), "foo text"));
	CPatchStr c(a);
	assert(stringMatch(c.ToStr(), "test data"));

	CPatchStr d(a.SubStr(3, 5));
	assert(stringMatch(d.ToStr(), "t dat"));
	d.Append(b);
	assert(stringMatch(d.ToStr(), "t datfoo text"));
	d.Append(b.SubStr(3, 4));
	assert(stringMatch(d.ToStr(), "t datfoo text tex"));
	c.Append(d);
	assert(stringMatch(c.ToStr(), "test datat datfoo text tex"));
	c.Append(c);
	assert(stringMatch(c.ToStr(), "test datat datfoo text textest datat datfoo text tex"));
	cout << d.ToStr() << endl;
	cout << c.SubStr(6, 9).ToStr() << endl;
	d.Insert(2, c.SubStr(6, 9));
	cout << d.ToStr() << endl;
	assert(stringMatch(d.ToStr(), "t atat datfdatfoo text tex"));

	b = "abcdefgh";
	assert(stringMatch(b.ToStr(), "abcdefgh"));
	assert(stringMatch(b.ToStr(), "abcdefgh"));
	assert(stringMatch(d.ToStr(), "t atat datfdatfoo text tex"));
	assert(stringMatch(d.SubStr(4, 8).ToStr(), "at datfd"));
	assert(stringMatch(b.SubStr(2, 6).ToStr(), "cdefgh"));
	try
	{
		b.SubStr(2, 7).ToStr();
		assert("Exception not thrown" == NULL);
	}
	catch (InvalidIndexException & e)
	{
	}
	catch (...)
	{
		assert("Invalid exception thrown" == NULL);
	}
	cout << a.ToStr() << endl;
	a.Delete(3, 5);
	cout << a.ToStr() << endl;
	assert(stringMatch(a.ToStr(), "tesa"));

	return 0;
}
#endif /* __PROGTEST__ */
