#ifndef __PROGTEST__
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <vector>
#include <set>
#include <map>
#include <list>
#include <algorithm>
#include <memory>
using namespace std;

struct CCoord
{
	CCoord(int x = 0, int y = 0) { m_X = x; m_Y = y; }
	int   m_X;
	int   m_Y;
};
#endif /* __PROGTEST__ */

class cmp
{
public:
	cmp(int y) : m_y(y) {}
	bool operator () (const CCoord & a, const CCoord & b)
	{
		if (a.m_Y < b.m_Y) return true;
		else if (a.m_Y == b.m_Y)
		{
			if (a.m_Y < m_y) {
				if (a.m_X < b.m_X) return true;
				return false;
			}
			else {
				if (a.m_X < b.m_X) return false;
				return true;
			}
		}
		return false;
	}

private:
	int m_y;
};

long long int rotate(CCoord A, CCoord B, CCoord C)
{
	return (B.m_X - A.m_X) * 1ll * (C.m_Y - B.m_Y) - (B.m_Y - A.m_Y) * 1ll * (C.m_X - B.m_X);
}

bool intersect(CCoord A, CCoord B, CCoord C, CCoord D)
{
	return rotate(A, B, C) * 1ll * rotate(A, B, D) <= 0 && rotate(C, D, A) * 1ll * rotate(C, D, B) < 0;
}

struct AABB
{
	AABB(int x0, int y0, int x1, int y1) : m_x0(x0), m_y0(y0), m_x1(x1), m_y1(y1) {}
	bool intersect(AABB boundary) const
	{
		return !(m_x0 > boundary.m_x1
			|| m_x1 < boundary.m_x0
			|| m_y1 < boundary.m_y0
			|| m_y0 > boundary.m_y1);
	}

	int m_x0, m_y0, m_x1, m_y1;
};


class CShape
{
public:
	virtual AABB get_boundary() const = 0;
	virtual bool contain(int x, int y) const = 0;
	virtual int get_id() const = 0;
	virtual shared_ptr<CShape> copy() const = 0;
};


class CPolygon : public CShape
{
public:
	CPolygon() {}
	CPolygon(int ID, int cnt, const CCoord * v) : m_id(ID)
	{

		int zero = 0;

		for (int i = 0; i < cnt; ++i)
		{
			min_x = min(min_x, v[i].m_X);
			min_y = min(min_y, v[i].m_Y);

			if (min_x == v[i].m_X) { zero = i; }

			max_x = max(max_x, v[i].m_X);
			max_y = max(max_y, v[i].m_Y);

			points.push_back(v[i]);
		}

		if (cnt > 0) {
			rotate(points.begin(), points.begin() + zero, points.end());
			sort(points.begin() + 1, points.end(), cmp(points[0].m_Y));
		}
	}

	AABB get_boundary(void) const
	{
		return AABB(min_x - 10, min_y - 10, max_x + 10, max_y + 10);
	}

	bool contain(int x, int y) const
	{
		CCoord A(x, y);
		int n = points.size();
		if (rotate(points[0], points[1], A) < 0 || rotate(points[0], points[n - 1], A) > 0) return false;

		int p = 1, r = n - 1;
		while (r - p > 1)
		{
			int q = (p + r) / 2;
			if (rotate(points[0], points[q], A) < 0) r = q;
			else p = q;
		}

		return !intersect(points[0], A, points[p], points[r]);
	}

	int get_id() const
	{
		return m_id;
	}

	virtual shared_ptr<CShape> copy() const { return make_shared<CPolygon>(*this); }

protected:
	int m_id;
	vector<CCoord> points;
	vector<CCoord> tmp;
	int min_x = 1048576, min_y = 1048576;
	int max_x = -1048576, max_y = -1048576;
};

class CRectangle : public CShape
{
public:
	CRectangle(int ID, int x1, int y1, int x2, int y2)
	{
		CCoord vertex[4] = { CCoord(x1, y1), CCoord(x1, y2), CCoord(x2, y1), CCoord(x2, y2) };
		p = CPolygon(ID, 4, vertex);
	}

	AABB get_boundary(void) const
	{
		return p.get_boundary();
	}

	bool contain(int x, int y) const
	{
		return p.contain(x, y);
	}

	int get_id() const
	{
		return p.get_id();
	}

	shared_ptr<CShape> copy() const { return make_shared<CRectangle>(*this); }

private:
	CPolygon p;
};

class CCircle : public CShape
{
public:
	CCircle(int ID, int x, int y, int r) : m_id(ID), m_x(x), m_y(y), m_r(r) {}

	AABB get_boundary() const
	{
		return AABB(m_x - m_r - 10, m_y - m_r - 10, m_x + m_r + 10, m_y + m_r + 10);
	}

	bool contain(int x, int y) const
	{
		return (x - m_x) * 1ll * (x - m_x) + (y - m_y) * 1ll * (y - m_y) <= m_r * 1ll * m_r;
	}

	int get_id() const
	{
		return m_id;
	}

	shared_ptr<CShape> copy() const { return make_shared<CCircle>(*this); }

private:
	int m_id;
	int m_x, m_y, m_r;
};


class CTriangle : public CShape
{
public:
	CTriangle(int ID, CCoord a, CCoord  b, CCoord c)
	{
		CCoord vertex[3] = { a, b, c };
		p = CPolygon(ID, 3, vertex);
	}

	AABB get_boundary(void) const
	{
		return p.get_boundary();
	}

	bool contain(int x, int y) const
	{
		return p.contain(x, y);
	}

	int get_id() const
	{
		return p.get_id();
	}

	shared_ptr<CShape> copy() const { return make_shared<CTriangle>(*this); }

private:
	CPolygon p;
};


class QuadTree
{
public:
	QuadTree(AABB boundary, size_t capacity) :m_boundary(boundary), m_capacity(capacity) {}
	void subdivide()
	{
		AABB box = m_boundary;
		int xMid = (box.m_x1 + box.m_x0) / 2;
		int yMid = (box.m_y1 + box.m_y0) / 2;

		AABB northWest = AABB(box.m_x0, box.m_y0, xMid, yMid);
		this->northWest = make_shared<QuadTree>(northWest, m_capacity);

		AABB northEast = AABB(xMid, box.m_y0, box.m_x1, yMid);
		this->northEast = make_shared<QuadTree>(northEast, m_capacity);

		AABB southWest = AABB(box.m_x0, yMid, xMid, box.m_y1);
		this->southWest = make_shared<QuadTree>(southWest, m_capacity);

		AABB southEast = AABB(xMid, yMid, box.m_x1, box.m_y1);
		this->southEast = make_shared<QuadTree>(southEast, m_capacity);
	}

	bool insert(const CShape & s)
	{
		if (!m_boundary.intersect(s.get_boundary()))
			return false;


		shared_ptr<CShape> tmp = s.copy();
		if (points.size() < m_capacity && northWest == NULL)
		{
			points.push_back(tmp);
			return true;
		}

		if (northWest == NULL) subdivide();
		if (northWest->insert(s)) return true;
		if (northEast->insert(s)) return true;
		if (southWest->insert(s)) return true;
		if (southEast->insert(s)) return true;

		return false;
	}

	vector<shared_ptr<CShape>> queryRange(AABB range) const
	{
		vector<shared_ptr<CShape>> pointsInRange;
		if (!m_boundary.intersect(range)) return pointsInRange;

		for (size_t p = 0; p < points.size(); p++)
		{
			if (range.intersect(points[p]->get_boundary()))
				pointsInRange.push_back(points[p]);
		}

		if (northWest == NULL) return pointsInRange;

		vector<shared_ptr<CShape>> tmp;
		tmp = northWest->queryRange(range);
		pointsInRange.insert(pointsInRange.end(), tmp.begin(), tmp.end());

		tmp = northEast->queryRange(range);
		pointsInRange.insert(pointsInRange.end(), tmp.begin(), tmp.end());

		tmp = southWest->queryRange(range);
		pointsInRange.insert(pointsInRange.end(), tmp.begin(), tmp.end());

		tmp = southEast->queryRange(range);
		pointsInRange.insert(pointsInRange.end(), tmp.begin(), tmp.end());

		return pointsInRange;
	}

private:
	AABB m_boundary;
	size_t m_capacity;
	vector<shared_ptr<CShape>> points;

	shared_ptr<QuadTree> northWest;
	shared_ptr<QuadTree> northEast;
	shared_ptr<QuadTree> southWest;
	shared_ptr<QuadTree> southEast;
};

bool unq(shared_ptr<CShape> a, shared_ptr<CShape> b)
{
	if (a->get_id() == b->get_id()) return true;
	return false;
}

class CScreen
{
public:
	CScreen() : tree(AABB(-1048576, -1048576, 1048576, 1048576), 100) {}
	void Add(const CShape & s)
	{
		tree.insert(s);
	}

	void Optimize(void)
	{

	}

	void Test(int x, int y, int & len, int * & list) const
	{
		AABB test(x - 10, y - 10, x + 10, y + 10);
		vector<shared_ptr<CShape>> result = tree.queryRange(test);
		auto it = unique(result.begin(), result.end(), unq);
		vector<shared_ptr<CShape>> result1(result.begin(), it);
		result = result1;

		int * res = new int[result.size()];

		int c = 0;
		for (auto it = result.begin(); it != result.end(); ++it)
		{
			if ((*it)->contain(x, y) == true) {
				res[c] = ((*it)->get_id());
				c++;
			}
		}

		len = c;
		if (len == 0) {
			delete res;
			list = NULL;
		}
		else list = res;
	}
private:
	QuadTree tree;
};

#ifndef __PROGTEST__
int main() {
	int   * res, resLen;

	CScreen  S0;
	S0.Add(CRectangle(1, 10, 20, 30, 40));
	S0.Add(CRectangle(2, 20, 10, 40, 30));
	S0.Add(CTriangle(3, CCoord(10, 20), CCoord(20, 10), CCoord(30, 30)));
	S0.Optimize();
	S0.Test(0, 0, resLen, res);
	cout << resLen << endl;
	// resLen = 0, res = [ ]
	delete[] res;

	S0.Test(21, 21, resLen, res);
	cout << resLen << endl;
	// resLen = 3, res = [ 1 2 3 ]
	delete[] res;

	S0.Test(30, 22, resLen, res);
	cout << resLen << endl;
	// resLen = 2, res = [ 1 2 ]
	delete[] res;

	S0.Test(16, 17, resLen, res);
	cout << resLen << endl;
	// resLen = 1, res = [ 3 ]
	delete[] res;

	S0.Test(35, 25, resLen, res);
	cout << resLen << endl;
	// resLen = 1, res = [ 2 ]
	delete[] res;


	CScreen  S1;
	S1.Add(CCircle(1, 10, 10, 15));
	S1.Add(CCircle(2, 30, 10, 15));
	S1.Add(CCircle(3, 20, 20, 15));
	S1.Optimize();
	S1.Test(0, 0, resLen, res);
	cout << resLen << endl;
	// resLen = 1, res = [ 1 ]
	delete[] res;
	S1.Test(15, 15, resLen, res);
	cout << resLen << endl;
	// resLen = 2, res = [ 1 3 ]
	delete[] res;
	S1.Test(20, 11, resLen, res);
	cout << resLen << endl;
	// resLen = 3, res = [ 1 2 3 ]
	delete[] res;
	S1.Test(32, 8, resLen, res);
	cout << resLen << endl;
	// resLen = 1, res = [ 2 ]
	delete[] res;


	CScreen  S2;
	CCoord  vertex1[4] = { CCoord(10, 0), CCoord(20, 20), CCoord(30, 20), CCoord(40, 0) };
	S2.Add(CPolygon(1, 4, vertex1));
	CCoord  vertex2[5] = { CCoord(20, 10), CCoord(10, 20), CCoord(25, 30), CCoord(40, 20), CCoord(30, 10) };
	S2.Add(CPolygon(2, 5, vertex2));
	S2.Optimize();
	S2.Test(25, 15, resLen, res);
	cout << resLen << endl;
	// resLen = 2, res = [ 1 2 ]
	cout << "[";
	for (int i = 0; i < resLen; i++) cout << res[i] << " ";
	cout << "]" << endl;
	delete[] res;
	S2.Test(25, 25, resLen, res);
	cout << resLen << endl;
	// resLen = 1, res = [ 2 ]
	cout << "[";
	for (int i = 0; i < resLen; i++) cout << res[i] << " ";
	cout << "]" << endl;
	delete[] res;
	S2.Test(15, 3, resLen, res);
	cout << resLen << endl;
	// resLen = 1, res = [ 1 ]
	cout << "[";
	for (int i = 0; i < resLen; i++) cout << res[i] << " ";
	cout << "]" << endl;
	delete[] res;
	S2.Test(11, 10, resLen, res);
	cout << resLen << endl;
	// resLen = 0, res = [ ]
	cout << "[";
	for (int i = 0; i < resLen; i++) cout << res[i] << " ";
	cout << "]" << endl;
	delete[] res;
	return 0;
}
#endif /* __PROGTEST__ */
