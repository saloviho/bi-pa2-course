#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <cassert>
#include <cmath>
#include <cctype>
#include <climits>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <list>
#include <algorithm>
#include <functional>
#include <memory>
using namespace std;

const uint16_t ENDIAN_LITTLE = 0x4949;
const uint16_t ENDIAN_BIG = 0x4d4d;

#endif /* __PROGTEST__ */

uint16_t decipher(uint16_t in) {
	uint16_t out = 0x0000;
	for (int i = 0; i < 8; i++) {
		out = out | (((in >> i) & 0x0001) << (8 + i));
	}

	out = out | (in >> 8);
	return out;
}

class Image {

public:
	bool readImage(const char * srcFileName) {
		// Opening file for reading
		ifstream input(srcFileName, ios::in | ios::binary);
		if (!input.is_open()) return false;

		//Reading header data
		input.read(p.endian, 2);
		if (!input.good()) return false;
		input.read(p.width, 2);
		if (!input.good()) return false;
		input.read(p.height, 2);
		if (!input.good()) return false;
		input.read(p.format, 2);
		if (!input.good()) return false;

		//Converting pointer into value
		endian = *((uint16_t *)(p.endian));
		if (endian != ENDIAN_LITTLE && endian != ENDIAN_BIG) return false;

		width = *((uint16_t *)(p.width));
		height = *((uint16_t *)(p.height));
		uint16_t format = *((uint16_t *)(p.format));

		if (endian == ENDIAN_BIG) {
			width = decipher(width);
			height = decipher(height);
			format = decipher(format);
		}

		if (width <= 0) return false;
		if (height <= 0) return false;

		//Getting channel count and bits per channel count
		uint16_t mask1 = 0b00000011;
		uint16_t mask2 = 0b00000111;

		channel = (format & mask1) + 1;
		if (channel == 2) return false;

		format = format >> 2;
		int exp = format & mask2;
		if (exp != 0 && exp != 3 && exp != 4) return false;
		bpc = (int)pow(2, exp);


		//Reading image data
		p.data = new Pixel *[height];
		p.allocated = 1;
		for (int i = 0; i < height; i++) {
			p.data[i] = new Pixel[width];
			for (int j = 0; j < width; j++) {
				p.data[i][j].allocPixel(channel, bpc);
				input.read(p.data[i][j].data, (int)(channel * bpc / 8));
				if (!input.good()) {
					freeMem(i);
					return false;
				}
			}
		}

		char * trash = new char[1];
		input.read(trash, 1);

		if (!input.eof()) {
			freeMem(height);
			return false;
		}
		return true;
	}

	bool writeImage(const char * dstFileName) {
		ofstream output(dstFileName, ios::out | ios::binary);
		if (!output.is_open()) {
			freeMem(height);
			return false;
		}
		output.write(p.endian, 2);
		if (!output.good()) {
			freeMem(height);
			return false;
		}
		output.write(p.width, 2);
		if (!output.good()) {
			freeMem(height);
			return false;
		}
		output.write(p.height, 2);
		if (!output.good()) {
			freeMem(height);
			return false;
		}
		output.write(p.format, 2);
		if (!output.good()) {
			freeMem(height);
			return false;
		}
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				output.write(p.data[i][j].data, (int)(channel * bpc / 8));
				if (!output.good()) {
					freeMem(i);
					return false;
				}
			}
		}
		return true;
	}

	void freeMem(int h) {
		if (p.allocated == 1) {
			for (int i = 0; i < h; i++) {
				delete[] p.data[i];
			}
			delete[] p.data;
			p.allocated = 0;
		}
	}

	void flipRow(int x, int y) {
		for (int i = 0; i < width; i++) {
			char * data = p.data[x][i].data;
			p.data[x][i] = p.data[y][i];
			p.data[y][i].data = data;
		}
	}

	void flipColumn(int x, int y) {
		for (int i = 0; i < height; i++) {
			char * data = p.data[i][x].data;
			p.data[i][x] = p.data[i][y];
			p.data[i][y].data = data;
		}
	}

	void flip(bool horizontal, bool vertical) {
		if (horizontal) {
			for (int i = 0; i < width / 2; i++) {
				flipColumn(i, width - i - 1);
			}
		}

		if (vertical) {
			for (int i = 0; i < height / 2; i++) {
				flipRow(i, height - i - 1);
			}
		}
	}

private:
	class Pixel {
	public:
		Pixel() {}

		Pixel(const Pixel & a) {
			data = new char[sizeof(a.data)];
			*data = *a.data;
			allocated = 1;
		}

		void allocPixel(int channel, int bpc) {
			int mem = (int)(channel * bpc / 8);
			data = new char[mem];
			allocated = 1;
		}

		~Pixel() {
			if (allocated == 1) delete[] data;
			allocated = 0;
		}

		int allocated = 0;
		char * data;
	};

	class Properties {
	public:
		Properties() {
			endian = new char[2];
			width = new char[2];
			height = new char[2];
			format = new char[2];
		}

		~Properties() {
			delete endian;
			delete width;
			delete height;
			delete format;
		}

		char * endian;
		char * width;
		char * height;
		char * format;
		Pixel ** data;
		int allocated = 0;
	};

	uint16_t endian, height, width, channel, bpc;
	Properties p;
};

bool flipImage(const char  * srcFileName,
	const char  * dstFileName,
	bool          flipHorizontal,
	bool          flipVertical)
{
	Image img1;
	if (!img1.readImage(srcFileName)) return false;
	img1.flip(flipHorizontal, flipVertical);
	if (!img1.writeImage(dstFileName)) return false;
	return true;
}

#ifndef __PROGTEST__
bool identicalFiles(const char * fileName1,
	const char * fileName2)
{
	ifstream file1(fileName1);
	ifstream file2(fileName2);
	if (!file1.is_open() || !file2.is_open()) return false;
	char data1, data2;
	while (!file1.eof() || !file2.eof()) {
		file1.read(&data1, sizeof(char));
		file2.read(&data2, sizeof(char));
		if (data1 != data2) {
			return false;
		}
	}

	if (!file1.eof() || !file2.eof()) {
		return false;
	}

	return true;
}

int main(void)
{

	assert(flipImage("input_00.img", "output_00.img", true, false)
		&& identicalFiles("output_00.img", "ref_00.img"));

	assert(flipImage("input_01.img", "output_01.img", false, true)
		&& identicalFiles("output_01.img", "ref_01.img"));

	assert(flipImage("input_02.img", "output_02.img", true, true)
		&& identicalFiles("output_02.img", "ref_02.img"));

	assert(flipImage("input_03.img", "output_03.img", false, false)
		&& identicalFiles("output_03.img", "ref_03.img"));

	assert(flipImage("input_04.img", "output_04.img", true, false)
		&& identicalFiles("output_04.img", "ref_04.img"));

	assert(flipImage("input_05.img", "output_05.img", true, true)
		&& identicalFiles("output_05.img", "ref_05.img"));

	assert(flipImage("input_06.img", "output_06.img", false, true)
		&& identicalFiles("output_06.img", "ref_06.img"));

	assert(flipImage("input_07.img", "output_07.img", true, false)
		&& identicalFiles("output_07.img", "ref_07.img"));

	assert(flipImage("input_08.img", "output_08.img", true, true)
		&& identicalFiles("output_08.img", "ref_08.img"));

	assert(!flipImage("input_09.img", "output_09.img", true, false));

	// extra inputs (optional & bonus tests)
	assert(flipImage("extra_input_00.img", "extra_out_00.img", true, false)
		&& identicalFiles("extra_out_00.img", "extra_ref_00.img"));

	assert(flipImage("extra_input_01.img", "extra_out_01.img", false, true)
		&& identicalFiles("extra_out_01.img", "extra_ref_01.img"));

	assert(flipImage("extra_input_02.img", "extra_out_02.img", true, false)
		&& identicalFiles("extra_out_02.img", "extra_ref_02.img"));

	assert(flipImage("extra_input_03.img", "extra_out_03.img", false, true)
		&& identicalFiles("extra_out_03.img", "extra_ref_03.img"));
	assert(flipImage("extra_input_04.img", "extra_out_04.img", true, false)
		&& identicalFiles("extra_out_04.img", "extra_ref_04.img"));
	assert(flipImage("extra_input_05.img", "extra_out_05.img", false, true)
		&& identicalFiles("extra_out_05.img", "extra_ref_05.img"));
	assert(flipImage("extra_input_06.img", "extra_out_06.img", true, false)
		&& identicalFiles("extra_out_06.img", "extra_ref_06.img"));
	assert(flipImage("extra_input_07.img", "extra_out_07.img", false, true)
		&& identicalFiles("extra_out_07.img", "extra_ref_07.img"));
	assert(flipImage("extra_input_08.img", "extra_out_08.img", true, false)
		&& identicalFiles("extra_out_08.img", "extra_ref_08.img"));
	assert(flipImage("extra_input_09.img", "extra_out_09.img", false, true)
		&& identicalFiles("extra_out_09.img", "extra_ref_09.img"));
	assert(flipImage("extra_input_10.img", "extra_out_10.img", true, false)
		&& identicalFiles("extra_out_10.img", "extra_ref_10.img"));
	assert(flipImage("extra_input_11.img", "extra_out_11.img", false, true)
		&& identicalFiles("extra_out_11.img", "extra_ref_11.img"));

	return 0;
}
#endif /* __PROGTEST__ */
