#ifndef Item_H
#define Item_H

#include <SDL2/SDL.h>

#include <string>
#include <sstream>
#include <vector>

#include "Blocks.h"

using namespace std;

class Object;

/*!
 * Class represents item
 */
class Item
{
public:
	/*!
	 * Constructor
	 * @params name Item name
	 * @params count Item count
	 * @params render Surface to draw
	 */
	Item(string name, int count, SDL_Surface * render);

	/*!
	 * Getter for name
	 * @return Returns item name
	 */
	string getName();

	/*!
	 * Getter for count
	 * @return Returns item count
	 */
	int getCount();

	/*!
	 * Sets item count to value
	 * @param value Count to be set
	 */
	void setCount(int value);

	/*!
	 * Getter for surface
	 * @return Returns item surface
	 */
	SDL_Surface * getSprite();

	/*!
	 * Return object to be placed on the map
	 * @param x X-Position
	 * @param y Y-Position
	 * @return Returns object
	 */
	shared_ptr<Object> put(int x, int y);

	/*!
	* Creates string representation of item
	* @return Returns string representation of item
	*/
	string toString();

protected:
	string name;		/*!< Item name */
	SDL_Surface * s;	/*!< Item surface */
	SDL_Surface * r;	/*!< Surface to draw */
	int count;			/*!< Item count */
};

#endif
