#ifndef Movable_H
#define Movable_H

#include <vector>
#include <memory>

#include "Object.h"
#include "Map.h"

using namespace std;

/*!
 * Class represents moving object
 */
class Movable : public Object
{
public:

	/*!
	 * Constructor
	 * @param surface Object surface
	 * @param render Surface to draw
	 * @param hp Monster hp
	 * @param speed Monster speed
	 * @param attack Monster attack
	 * @param eyeSight Distance on which monster start following player
	 * @param attackRange Monster attack range
	 * @param x X-Position
	 * @param y Y-Position
	 * @param w Monster width
	 * @param h Monster height
	 */

	Movable(SDL_Surface * surface, SDL_Surface * render, int hp, float speed, int attack, int x, int y, int w, int h);
	
	/*!
	 * Step left
	 */
	void left();

	/*!
	* Step right
	*/
	void right();

	/*!
	* Jump
	*/
	void up();

	/*!
	* Attacks object
	* @param object Object to be attacked
	*/
	void attack(shared_ptr<Object> object);


	/*!
	* Moves an object
	* @param time Time past
	* @param map Map
	*/
	void Move(int time, shared_ptr<Map> m);

	/*!
	* Solves object collisions
	* @param dir Collision direction
	* @param m Map
	*/
	void Collision(int dir, shared_ptr<Map> m);

protected:
	bool onGround;			/*!< Is object on ground or not */
	int damage;				/*!< Object damage */
	int state;				/*!< Object state */
	float dx;				/*!< Current X-Axis speed */
	float dy;				/*!< Current Y-Axis speed */
	float speed;			/*!< Object movespeed */
	float currentFrame;		/*!< Current animation frame */
	SDL_Rect last;			/*!< Object last position */
};

#endif