#include "Zombie.h"

Zombie::Zombie(int x, int y, SDL_Surface * render): Monster(TextureManager::zombieSurface, render, 3, 0.5, 1, 300, 50, x, y, 60, 140) { }

string Zombie::toString()
{
	string result;
	result += "Z:" + getInfo();
	return result;
}
shared_ptr<Item> Zombie::getDrop() { return NULL; }