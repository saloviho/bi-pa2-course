#ifndef Map_H
#define Map_H

#include <SDL2/SDL.h>

#include <string>
#include <iostream>
#include <fstream>
#include <deque>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <sstream>
#include <memory>

#include "Object.h"

using namespace std;

class Monster;

/*!
 * Class represents map
 */

class Map
{
public:
	/*!
	* Constructor
	* @param lb Left border
	* @param rb Right border
	*/
	Map(int lb, int rb);

	/*!
	* Draws map
	* @param time Past time
	* @param offsetX X-Axis offset
	* @param offsetX Y-Axis offset
	*/
	void draw(int time, int offsetX, int offsetY);

	/*!
	* Adds column to the left of the map
	* @param column Column of tiles
	*/
	void addColumnBefore(vector<shared_ptr<Object>> column);
	
	/*!
	* Adds column to the right of the map
	* @param column Column of tiles
	*/
	void addColumnAfter(vector<shared_ptr<Object>> column);
	
	/*!
	* Creates string representation of map
	* @return Returns string representation of map
	*/
	string toString();

	deque<vector<shared_ptr<Object>>> map;	/*!< Collection of tiles */
	int ground;								/*!< Ground level */
	int left_border;						/*!< Left map border */
	int right_border;						/*!< Right map border */
};

#endif
