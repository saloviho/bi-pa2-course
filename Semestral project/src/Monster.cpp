#include "Monster.h"

Monster::Monster(SDL_Surface * surface, SDL_Surface * render, int hp, float speed, int attack, float eyeSight, float attackRange, int x, int y, int w, int h)
		: Movable(surface, render, hp, speed, attack, x, y, w, h), eyeSight(eyeSight), attackRange(attackRange) { }

void Monster::follow(shared_ptr<Player> p)
{
	if (alive)
	{
		float x = (p->getPos().x + p->getPos().w / 2.0 - pos.x - pos.w / 2.0) * (p->getPos().x + p->getPos().w / 2.0 - pos.x - pos.w / 2.0);
		float y = (p->getPos().y + p->getPos().h / 2.0 - pos.y - pos.h / 2.0) * (p->getPos().y + p->getPos().h / 2.0 - pos.y - pos.h / 2.0);

		if (sqrt(x) < eyeSight)
		{
			if (p->getPos().x < pos.x) left();
			if (p->getPos().x > pos.x) right();
		}

		if (sqrt(x + y) < attackRange)
		{
			if (state != 4)
			{
				attack(p);
				state = 4;
			}
		}
	}
}

void Monster::draw(int time, int offsetX, int offsetY, shared_ptr<Map> m)
{
	if (alive)
	{
		Move(time, m);
		currentFrame += 0.05;
		if (currentFrame >= 5)
		{
			currentFrame -= 5;
			state = 0;
		}

		dst.x = pos.x - offsetX;
		dst.y = pos.y - offsetY;
		dst.w = pos.w;
		dst.h = pos.h;
		dx = 0;

		TextureManager::Render(r, s, NULL, &dst);
	}
}
