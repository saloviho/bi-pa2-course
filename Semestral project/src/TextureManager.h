#include <SDL2/SDL.h>

/*!
* Utility class
* Helps to load/display objects, also checks object intersection
*/
class TextureManager
{
public:
	/*!
	* Grass texture
	*/
	static SDL_Surface * grassSurface;

	/*!
	* Gold texture
	*/
	static SDL_Surface * goldSurface;

	/*!
	* Dirt texture
	*/
	static SDL_Surface * dirtSurface;

	/*!
	* Redstone texture
	*/

	static SDL_Surface * redstoneSurface;
	
	/*!
	* Coal texture
	*/
	static SDL_Surface * coalSurface;
	/*!
	* Stone texture
	*/
	static SDL_Surface * stoneSurface;
	
	/*!
	* Leaves texture
	*/
	static SDL_Surface * leavesSurface;
	
	/*!
	* Zombie texture
	*/
	static SDL_Surface * zombieSurface;
	
	/*!
	* Zombie-pig texture
	*/
	static SDL_Surface * zombiePigSurface;
	
	/*!
	* Player texture
	*/
	static SDL_Surface * playerSurface;

	/*!
	* Hearth texture
	*/
	static SDL_Surface * hearthSurface;

	/*!
	* Selected item background
	*/
	static SDL_Surface * selectedSurface;

	/*!
	* Item background
	*/
	static SDL_Surface * notSelectedSurface;

	/*!
	* Method loads object texture
	* @param filename Name of file where texture is stored
	* @return Returns texture surface
	*/
	static SDL_Surface * LoadTexture(const char * filename);
	
	/*!
	* Displays texture
	* @param window Surface to draw
	* @param tex Surface of texture to be drawn
	* @param src Source rectangle
	* @param dst Destination rectangle
	*/
	static void Render(SDL_Surface * window, SDL_Surface * tex, SDL_Rect * src, SDL_Rect * dst);
	
	/*!
	* Checks rectangle intersection
	* @param a First rectangle
	* @param b Second rectangle
	* @return Returns true if intersect
	*/
	static bool Intersection(SDL_Rect a, SDL_Rect b);
};
