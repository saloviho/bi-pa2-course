#include "Game.h"

Game::Game()
{
	SDL_Init(SDL_INIT_EVERYTHING);
	window = SDL_CreateWindow("My Game", 100, 100, 800, 640, 0);
	windowSurface = SDL_GetWindowSurface(window);
	map = make_shared<Map>(0, 1);
	p = make_shared<Player>(100, 100, windowSurface);
	gen.generateArea(0, map, enemy, windowSurface);
	gen.generateArea(1, map, enemy, windowSurface);
}

bool Game::save()
{
	ofstream savefile;
	stringstream ss;
	savefile.open("save.txt", ios::out);
	if (savefile.is_open())
	{
		savefile << map->toString();
		if (!savefile.good()) return false;
		
		ss << enemy.size();
		savefile << ss.str() + "\n";
		if (!savefile.good()) return false;

		for (size_t i = 0; i < enemy.size(); i++)
		{
			savefile << enemy[i]->toString() + "\n";
			if (!savefile.good()) return false;
		}
		
		savefile << p->toString();
		if (!savefile.good()) return false;
		savefile.close();
		return true;
	}
	
	return false;
}

void Game::clean()
{
	SDL_FreeSurface(TextureManager::coalSurface);
	SDL_FreeSurface(TextureManager::dirtSurface);
	SDL_FreeSurface(TextureManager::redstoneSurface);
	SDL_FreeSurface(TextureManager::leavesSurface);
	SDL_FreeSurface(TextureManager::goldSurface);
	SDL_FreeSurface(TextureManager::grassSurface);
	SDL_FreeSurface(TextureManager::stoneSurface);
	SDL_FreeSurface(TextureManager::playerSurface);
	SDL_FreeSurface(TextureManager::zombieSurface);
	SDL_FreeSurface(TextureManager::zombiePigSurface);
	SDL_FreeSurface(TextureManager::hearthSurface);
	SDL_FreeSurface(TextureManager::selectedSurface);
	SDL_FreeSurface(TextureManager::notSelectedSurface);
	SDL_FreeSurface(windowSurface);
	SDL_DestroyWindow(window);
}

bool Game::load()
{	
	ifstream savefile("save.txt");
	if (savefile.is_open())
	{
		string line;
		getline(savefile, line);
		vector<string> data = parse(line, ':');
		int lb, rb, ms;
		lb = stoi(data[0]);
		rb = stoi(data[1]);
		ms = stoi(data[2]);

		map = make_shared<Map>(lb, rb);
		for (int i = 0; i < ms; i++)
		{
			vector<shared_ptr<Object>> column;
			for (int j = 0; j < 20; j++)
			{
				shared_ptr<Object> block;
				getline(savefile, line);
				data = parse(line, ':');

				if (data[0] == "0") block = NULL;
				if (data[0] == "L") block = make_shared<Leaves>(stoi(data[1]), stoi(data[2]), windowSurface);
				if (data[0] == "S") block = make_shared<Stone>(stoi(data[1]), stoi(data[2]), windowSurface);
				if (data[0] == "R") block = make_shared<Redstone>(stoi(data[1]), stoi(data[2]), windowSurface);
				if (data[0] == "D") block = make_shared<Dirt>(stoi(data[1]), stoi(data[2]), windowSurface);
				if (data[0] == "Z") block = make_shared<Gold>(stoi(data[1]), stoi(data[2]), windowSurface);
				if (data[0] == "G") block = make_shared<Grass>(stoi(data[1]), stoi(data[2]), windowSurface);
				if (data[0] == "C") block = make_shared<Coal>(stoi(data[1]), stoi(data[2]), windowSurface);

				if (block != NULL) block->getDmg(block->getHP() - stoi(data[3]));
				column.push_back(block);
			}
			map->map.push_back(column);
		}

		getline(savefile, line);
		int es = stoi(line);

		enemy.clear();
		enemy.resize(es);
		for (int i = 0; i < es; i++)
		{
			shared_ptr<Monster> monster;
			getline(savefile, line);
			data = parse(line, ':');
			if (data[0] == "Z") monster = make_shared<Zombie>(stoi(data[1]), stoi(data[2]), windowSurface);
			if (data[0] == "ZP") monster = make_shared<ZombiePig>(stoi(data[1]), stoi(data[2]), windowSurface);
			monster->getDmg(monster->getHP() - stoi(data[3]));
			enemy[i] = monster;
		}

		getline(savefile, line);
		data = parse(line, ':');

		p = make_shared<Player>(stoi(data[0]), stoi(data[1]), windowSurface);
		p->getDmg(p->getHP() - stoi(data[2]));
		for (int i = 0; i < 6; i++)
		{
			shared_ptr<Item> item;
			getline(savefile, line);
			data = parse(line, ':');
			if (data[0] == "0") item = NULL;
			else item = make_shared<Item>(data[0], stoi(data[1]), windowSurface);
			p->harvest(item);
		}
		savefile.close();
		return true;
	}
	return false;
}

vector<string> Game::parse(string str, char delimiter)
{
	std::vector<string> tokens;
	std::string token;
	std::istringstream tokenStream(str);
	while (getline(tokenStream, token, delimiter))
	{
		tokens.push_back(token);
	}
	return tokens;
}

void Game::leftClick()
{	
	int a, b;
	SDL_GetMouseState(&a, &b);

	int x = a + offsetX;
	int y = b + offsetY;

	SDL_Rect hitbox;
	hitbox.x = x;
	hitbox.y = y;
	hitbox.w = 1;
	hitbox.h = 1;

	vector<shared_ptr<Object>> collisions;

	int rangeX = ((map->left_border + 1) * -80 + p->getPos().x) / 80;
	int rangeY = p->getPos().y / 80;
	
	for (int i = rangeX - 1; i < rangeX + 3; i++)
	{
		for (int j = rangeY - 1; j < rangeY + 3; j++)
		{
			if (j > 0 && j < 19)
			{
				if (map->map[i][j] == NULL) continue;
				if (TextureManager::Intersection(hitbox, map->map[i][j]->getPos()))
				{
					collisions.push_back(map->map[i][j]);
				}
			}
		}

		for (size_t i = 0; i < enemy.size(); i++)
		{
			if (TextureManager::Intersection(hitbox, enemy[i]->getPos()))
			{
				collisions.push_back(enemy[i]);
			}
		}
	}

	p->hit(collisions);
}

void Game::rightClick()
{
	int a, b;
	SDL_GetMouseState(&a, &b);

	size_t x = (a + offsetX - (map->left_border + 1) * 80) / 80;
	size_t y = (b + offsetY) / 80;

	if (x >= 0 && x < map->map.size() && y >= 0 && y < 20)
	{
		if (map->map[x][y] == NULL)
		{
			p->place(map, x, y);
		}
	}

}

void Game::start()
{
	int frameDelay = 1000/120;
	int frameTime = 0;
	SDL_Event event;

	while (true)
	{
		int time = SDL_GetTicks();
		SDL_PollEvent(&event);

		if (event.type == SDL_QUIT) break;
		if (event.type == SDL_KEYDOWN)
		{
			if (event.key.keysym.sym == SDLK_r)
			{
				offsetX = 0; offsetY = 0;
				p = make_shared<Player>(100, 100, windowSurface);
				map = make_shared<Map>(0, 1);

				enemy.clear();

				gen.generateArea(0, map, enemy, windowSurface);
				gen.generateArea(1, map, enemy, windowSurface);
			}

			if (event.key.keysym.sym == SDLK_l)
			{
				if (load()) cout << "Loaded" << endl;
				else cout << "Loading failed" << endl;
			}

			if (p->isAlive())
			{
				if (event.key.keysym.sym == SDLK_s)
				{
					if (save()) cout << "Saved" << endl;
					else cout << "Saving failed" << endl;
				}

				if (event.key.keysym.sym == SDLK_F1)
				{
					p->selectItem(0);
				}

				if (event.key.keysym.sym == SDLK_F2)
				{
					p->selectItem(1);
				}

				if (event.key.keysym.sym == SDLK_F3)
				{
					p->selectItem(2);
				}

				if (event.key.keysym.sym == SDLK_F4)
				{
					p->selectItem(3);
				}

				if (event.key.keysym.sym == SDLK_F5)
				{
					p->selectItem(4);
				}

				if (event.key.keysym.sym == SDLK_F6)
				{
					p->selectItem(5);
				}

				if (event.key.keysym.sym == SDLK_w)
				{
					p->up();
				}

				if (event.key.keysym.sym == SDLK_a)
				{
					p->left();
				}

				if (event.key.keysym.sym == SDLK_d)
				{
					p->right();
				}
			}
		}

		if(p->isAlive())
		{
				Uint8 mouse_state = SDL_GetMouseState(NULL, NULL);
				if (mouse_state & SDL_BUTTON_LMASK)
				{
					leftClick();
				}

				if (mouse_state & SDL_BUTTON_RMASK)
				{
					rightClick();
				}
		}

		SDL_FillRect(windowSurface, NULL, SDL_MapRGB(windowSurface->format, 255, 255, 255));
		offsetX = p->getPos().x - 400;
		if (p->getPos().y > 320 && p->getPos().y < 1600 - 320) offsetY = p->getPos().y - 320;

		if ((p->getPos().x - 500) / 80 < map->left_border) gen.generateArea(0, map, enemy, windowSurface);
		if ((p->getPos().x + 500) / 80 > map->right_border) gen.generateArea(1, map, enemy, windowSurface);

		map->draw(frameDelay, offsetX, offsetY);
			
		for (auto it = enemy.begin(); it != enemy.end(); ++it)
		{
			if ((*it)->isAlive() == false) it = enemy.erase(it);
			else
			{
				(*it)->follow(p);
				(*it)->draw(frameDelay, offsetX, offsetY, map);
			}
			if (it == enemy.end()) break;
		}
			
		p->draw(frameDelay, offsetX, offsetY, map);
		p->drawi();

		SDL_UpdateWindowSurface(window);
		frameTime = SDL_GetTicks() - time;
		if (frameDelay > frameTime) SDL_Delay(frameDelay - frameTime);
	}
	clean();
}
