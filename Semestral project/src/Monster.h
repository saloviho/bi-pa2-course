#ifndef MONSTER_H
#define MONSTER_H

#include <SDL2/SDL.h>

#include <vector>

#include "Movable.h"
#include "Player.h"


/*!
 * Class represents monster
 */
class Monster : public Movable
{
public:
	/*!
	 * Constructor
	 * @param surface Object surface
	 * @param render Surface to draw
	 * @param hp Monster hp
	 * @param speed Monster speed
	 * @param attack Monster attack
	 * @param eyeSight Distance on which monster start following player
	 * @param attackRange Monster attack range
	 * @param x X-Position
	 * @param y Y-Position
	 * @param w Monster width
	 * @param h Monster height
	 */
	Monster(SDL_Surface * surface, SDL_Surface * render, int hp, float speed, int attack, float eyeSight, float attackRange, int x, int y, int w, int h);
	
	/*!
	 * Method allows monster to follow player and attack him
	 * @param p Player
	 */
	void follow(shared_ptr<Player> p);

	/*!
	 * Method draws object on screen
	 * @param time Past time
	 * @param offsetX X-Axis offset
	 * @param offsetY Y-Axis offset
	 * @param map Map to solve collisions
	 */
	void draw(int time, int offsetX, int offsetY, shared_ptr<Map> m);

private:
	float eyeSight;		/*!< Distance on which monster start following player */
	float attackRange;	/*!< Attack range */
};
#endif
