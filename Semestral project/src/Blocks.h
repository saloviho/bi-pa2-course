#ifndef Blocks_H
#define Blocks_H

#include <memory>

#include "Object.h"
#include "Item.h"
using namespace std;


/*!
 * Class represents grass block
 */

class Grass : public Object
{
public:

	/*!
	 * Constructor
	 * @param x X-Position of block
	 * @param y Y-Position of block
	 * @param render Destination surface
	 */
	Grass(int x, int y, SDL_Surface * render);
	
	/*!
	 * Method draws object on screen
	 * @param time Past time
	 * @param offsetX X-Axis offset
	 * @param offsetY Y-Axis offset
	 * @param map Map to solve collisions
	 */
	void draw(int time, int offsetX, int offsetY, shared_ptr<Map> m = NULL);
	
	/*!
	 * Method returns item which drops after block destroying or enemy killing
	 * @return Returns item as reward for killing
	 */
	shared_ptr<Item> getDrop();

	/*!
	* Creates string representation of an object
	* @return Returns string representation of an object
	*/
	string toString();
};

/*!
 * Class represents dirt block
 */
class Dirt : public Object
{
public:
	/*!
	 * Constructor
	 * @param x X-Position of block
	 * @param y Y-Position of block
	 * @param render Destination surface
	 */

	Dirt(int x, int y, SDL_Surface * render);

	/*!
	 * Method draws object on screen
	 * @param time Past time  
	 * @param offsetX X-Axis offset
	 * @param offsetY Y-Axis offset
	 * @param map Map to solve collisions
	 */
	void draw(int time, int offsetX, int offsetY, shared_ptr<Map> m = NULL);
	
	/*!
	 * Method returns item which drops after block destroying or enemy killing
	 * @return Returns item as reward for killing
	 */
	shared_ptr<Item> getDrop();

	/*!
	* Creates string representation of an object
	* @return Returns string representation of an object
	*/
	string toString();
};

/*!
 * Class represents stone block
 */

class Stone : public Object
{
public:
	/*!
	 * Constructor
	 * @param x X-Position of block
	 * @param y Y-Position of block
	 * @param render Destination surface
	 */
	Stone(int x, int y, SDL_Surface * render);
	
	/*!
	 * Method draws object on screen
	 * @param time Past time
	 * @param offsetX X-Axis offset
	 * @param offsetY Y-Axis offset
	 * @param map Map to solve collisions
	 */
	void draw(int time, int offsetX, int offsetY, shared_ptr<Map> m = NULL);
	
	/*!
	 * Method returns item which drops after block destroying or enemy killing
	 * @return Returns item as reward for killing
	 */
	shared_ptr<Item> getDrop();

	/*!
	* Creates string representation of an object
	* @return Returns string representation of an object
	*/
	string toString();
};

/*!
 * Class represents gold block
 */

class Gold : public Object
{
public:
	/*!
	 * Constructor
	 * @param x X-Position of block
	 * @param y Y-Position of block
	 * @param render Destination surface
	 */

	Gold(int x, int y, SDL_Surface * render);
	
	/*!
	 * Method draws object on screen
	 * @param time Past time
	 * @param offsetX X-Axis offset
	 * @param offsetY Y-Axis offset
	 * @param map Map to solve collisions
	 */
	void draw(int time, int offsetX, int offsetY, shared_ptr<Map> m = NULL);
	
	/*!
	 * Method returns item which drops after block destroying or enemy killing
	 * @return Returns item as reward for killing
	 */
	shared_ptr<Item> getDrop();

	/*!
	* Creates string representation of an object
	* @return Returns string representation of an object
	*/
	string toString();
};

/*!
 * Class represents redstone block
 */

class Redstone : public Object
{
public:

	/*!
	 * Constructor
	 * @param x X-Position of block
	 * @param y Y-Position of block
	 * @param render Destination surface
	 */
	Redstone(int x, int y, SDL_Surface * render);
	
	/*!
	 * Method draws object on screen
	 * @param time Past time
	 * @param offsetX X-Axis offset
	 * @param offsetY Y-Axis offset
	 * @param map Map to solve collisions
	 */
	
	void draw(int time, int offsetX, int offsetY, shared_ptr<Map> m = NULL);
	
	/*!
	 * Method returns item which drops after block destroying or enemy killing
	 * @return Returns item as reward for killing
	 */
	shared_ptr<Item> getDrop();

	/*!
	* Creates string representation of an object
	* @return Returns string representation of an object
	*/
	string toString();
};

/*!
 * Class represents coal block
 */

class Coal : public Object
{
public:

	/*!
	 * Constructor
	 * @param x X-Position of block
	 * @param y Y-Position of block
	 * @param render Destination surface
	 */
	Coal(int x, int y, SDL_Surface * render);
	
	/*!
	 * Method draws object on screen
	 * @param time Past time
	 * @param offsetX X-Axis offset
	 * @param offsetY Y-Axis offset
	 * @param map Map to solve collisions
	 */
	
	void draw(int time, int offsetX, int offsetY, shared_ptr<Map> m = NULL);
	
	/*!
	 * Method returns item which drops after block destroying or enemy killing
	 * @return Returns item as reward for killing
	 */
	
	shared_ptr<Item> getDrop();
	
	/*!
	* Creates string representation of an object
	* @return Returns string representation of an object
	*/

	string toString();
};

/*!
 * Class represents leaves block
 */

class Leaves : public Object
{
public:

	/*!
	 * Constructor
	 * @param x X-Position of block
	 * @param y Y-Position of block
	 * @param render Destination surface
	 */
	Leaves(int x, int y, SDL_Surface * render);
	
	/*!
	 * Method draws object on screen
	 * @param time Past time
	 * @param offsetX X-Axis offset
	 * @param offsetY Y-Axis offset
	 * @param map Map to solve collisions
	 */

	void draw(int time, int offsetX, int offsetY, shared_ptr<Map> m = NULL);
	
	/*!
	 * Method returns item which drops after block destroying or enemy killing
	 * @return Returns item as reward for killing
	 */
	
	shared_ptr<Item> getDrop();

	/*!
	* Creates string representation of an object
	* @return Returns string representation of an object
	*/

	string toString();
};

#endif