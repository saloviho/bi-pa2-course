#ifndef Generator_H
#define Generator_H

#include <vector>

#include "Object.h"
#include "Blocks.h"
#include "Zombie.h"
#include "ZombiePig.h"
#include "Map.h"


/*!
 * Class represents map generator
 */
class Generator
{
public:
	/*!
	* Method which generates map area
	* @params direction Defines direction of generating (0 - right, 1 - left)
	* @params map Map where generated tiles will be added
	* @params enemy Array where generated enemys will be added
	* @params render Surface to draw
	*/
	void generateArea(int direction, shared_ptr<Map> map, vector<shared_ptr<Monster>> & enemy, SDL_Surface * render);
};

#endif