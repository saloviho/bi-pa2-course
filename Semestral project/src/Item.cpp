#include "Item.h"

Item::Item(string name, int count, SDL_Surface * render) : name(name), count(count)
{
	r = render;
	if (name == "Dirt")
	{
		s = TextureManager::dirtSurface;
	}

	if (name == "Redstone")
	{
		s = TextureManager::redstoneSurface;
	}

	if (name == "Stone")
	{
		s = TextureManager::stoneSurface;
	}

	if (name == "Coal")
	{
		s = TextureManager::coalSurface;
	}

	if (name == "Gold")
	{
		s = TextureManager::goldSurface;
	}
}

shared_ptr<Object> Item::put(int x, int y)
{
	if (count <= 0) return NULL;
	else count--;

	if (name == "Dirt") return make_shared<Dirt>(x, y, r);
	if (name == "Redstone") return make_shared<Redstone>(x, y, r);
	if (name == "Stone") return make_shared<Stone>(x, y, r);
	if (name == "Coal") return make_shared<Coal>(x, y, r);
	if (name == "Gold") return make_shared<Gold>(x, y, r);
	return NULL;
}

string Item::toString()
{
	string result;
	ostringstream ss;
	ss << count;
	result = result + name + ":" + ss.str();
	return result;
}

int		Item::getCount() { return count; }
void		Item::setCount(int value) { count = value; }
string		Item::getName() { return name; }
SDL_Surface *	Item::getSprite() { return s; }
