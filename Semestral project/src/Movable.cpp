#include "Movable.h"

Movable::Movable(SDL_Surface * surface, SDL_Surface * render, int hp, float speed, int attack, int x, int y, int w, int h)
		: Object(surface, render, hp, x, y, w, h), onGround(false), damage(attack), state(0), dx(0), dy(0), speed(speed), currentFrame(0)
{
	last.x = x;
	last.y = y;
	last.w = w;
	last.h = h;
}

void Movable::left()
{
	if (state == 0)
	{
		currentFrame = 0;
		state = 1;
	}
	dx = -speed;
}

void Movable::right()
{
	if (state == 0)
	{
		currentFrame = 0;
		state = 1;
	}
	dx = speed;
}

void Movable::attack(shared_ptr<Object> enemy)
{
	enemy->getDmg(damage);
}

void Movable::up()
{
	if (onGround)
	{
		dy -= 1;
		onGround = false;
	}
}

void Movable::Move(int time, shared_ptr<Map> m)
{
	pos.x += (int)(dx * time);
	Collision(0, m);
	if (!onGround) dy += (0.002 * time);
	pos.y += (int)(dy * time);
	onGround = false;
	Collision(1, m);
}

void Movable::Collision(int dir, shared_ptr<Map> m)
{
	for (size_t i = 0; i < m->map.size(); i++)
	{
		for (size_t j = 0; j < 20; j++)
		{
			if (m->map[i][j] != NULL)
			{
				if (TextureManager::Intersection(pos, m->map[i][j]->getPos()))
				{
					if (dir == 0) pos.x = last.x;
					if (dir == 1)
					{
						pos.y = last.y;
						if (dy > 0)
						{
							int damage = (int)(dy / 0.5);
							if (damage > 0) getDmg(damage);
							onGround = true;
						}
						dy = 0;
					}
				}
			}
		}
	}

	last = pos;
}
