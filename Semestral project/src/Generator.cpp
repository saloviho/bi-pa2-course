#include "Generator.h"

void Generator::generateArea(int direction, shared_ptr<Map> map, vector<shared_ptr<Monster>> & enemy, SDL_Surface * render)
{
	int x;
	srand(time(0));
	for (int i = 0; i < 5; i++)
	{
		if (direction == 0) x = map->left_border;
		else x = map->right_border;

		vector<shared_ptr<Object>> column;
		column.resize(20);
		for (int j = 0; j < 20; j++)
		{
			if (j == 0 || j == 19) column[j] = make_shared<Leaves>(x * 80, j * 80, render);
			else if (j < map->ground) column[j] = NULL;
			else if (j == map->ground) column[j] = make_shared<Grass>(x * 80, j * 80, render);
			else if (j > map->ground && j < map->ground + 2) column[j] = make_shared<Dirt>(x * 80, j * 80, render);
			else {
				int r = rand() % 100;

				if (r >= 0 && r < 2) column[j] = make_shared<Gold>(x * 80, j * 80, render);
				else if (r >= 2 && r < 7) column[j] = make_shared<Redstone>(x * 80, j * 80, render);
				else if (r >= 7 && r < 17) column[j] = make_shared<Coal>(x * 80, j * 80, render);
				else if (r >= 17 && r < 58) column[j] = make_shared<Stone>(x * 80, j * 80, render);
				else column[j] = make_shared<Dirt>(x * 80, j * 80, render);
			}
		}

		int r = rand() % 100;
		if (r < 15)
		{
			int r1 = rand() % 3;
			if (r1 > 1) enemy.push_back(make_shared<Zombie>(x * 80, 100, render));
			else enemy.push_back(make_shared<ZombiePig>(x * 80, 100, render));
		}

		if (direction == 0)
		{
			map->addColumnBefore(column);
			map->left_border--;
		}
		else {
			map->addColumnAfter(column);
			map->right_border++;
		}
	}
}