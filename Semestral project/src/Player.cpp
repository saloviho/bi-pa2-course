#include "Player.h"

Player::Player(int x, int y, SDL_Surface * render) : Movable(TextureManager::playerSurface, render, 10, 0.7, 1, x, y, 115, 115), selectedItem(0) { }

void Player::place(shared_ptr<Map> map, int x, int y)
{
	shared_ptr<Object> block = NULL;
	shared_ptr<Item> item = inv.getItem(selectedItem);
	if (item != NULL) map->map[x][y] = item->put((map->left_border + 1) * 80 + x * 80 , y * 80);
}

void Player::harvest(shared_ptr<Item> item)
{
	inv.insert(item);
}

void Player::hit(vector<shared_ptr<Object>> collisions)
{
	state = 4;
	currentFrame = 0;
	for (size_t i = 0; i < collisions.size(); i++)
	{
		attack(collisions[i]);
		if (collisions[i]->isAlive() == false) harvest(collisions[i]->getDrop());
	}
}

void Player::getDmg(int value)
{
	if (alive)
	{
		currentFrame = 0;
		state = 2;
		Object::getDmg(value);
		if (!alive) state = 3;
	}
}

void Player::draw(int time, int offsetX, int offsetY, shared_ptr<Map> m)
{
	Move(time, m);
	if((state != 3 && currentFrame < 5) || (state == 3 && currentFrame < 4)) currentFrame += 0.05;
	if (currentFrame >= 5)
	{
		currentFrame -= 5;
		state = 0;
	}

	src.x = 115 * (int)currentFrame;
	src.w = 115;

	src.y = state * 115;
	src.h = 115;

	dst.x = pos.x - offsetX;
	dst.y = pos.y - offsetY;
	dx = 0;
	TextureManager::Render(r, s, &src, &dst);
}


string Player::toString()
{
	string result;
	result = getInfo() + "\n";
	result += inv.toString();
	return result;
}

void Player::drawi()
{
	SDL_Rect pp;
	pp.w = 25;
	pp.h = 25;

	for (int i = 0; i < hp; i++)
	{
		pp.x = 95 + i * 32;
		pp.y = 500;
		TextureManager::Render(r, TextureManager::hearthSurface, NULL, &pp);
	}
	inv.draw(selectedItem, r);
}

int			Player::getSelectedItem() { return selectedItem; }
void			Player::selectItem(size_t n) { selectedItem = n; }
shared_ptr<Item>	Player::getItem() { return inv.getItem(selectedItem); }
shared_ptr<Item>	Player::getDrop() { return NULL; }
