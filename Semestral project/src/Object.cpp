#include "Object.h"

Object::Object(SDL_Surface * surface, SDL_Surface * render, int hp, int x, int y, int w, int h) : hp(hp), alive(true)
{
	r = render;
	s = surface;

	pos.x = x;
	pos.y = y;
	pos.w = w;
	pos.h = h;

	src.x = 0;
	src.y = 0;
	src.w = w;
	src.h = h;

	dst.w = w;
	dst.h = h;
}

string	 Object::getInfo()
{
	string result;
	stringstream ss;

	ss << pos.x;
	string x = ss.str();
	ss.str(string());

	ss << pos.y;
	string y = ss.str();
	ss.str(string());

	ss << hp;
	string health = ss.str();
	
	result = x + ":" + y + ":" + health;
	return result;
}

void Object::getDmg(int value)
{
	if (alive)
	{
		hp = hp - value;
		if (hp <= 0) alive = false;
	}
}

int	 Object::getHP() { return hp; }
bool	 Object::isAlive() { return alive; }
SDL_Rect Object::getPos() { return pos; }
