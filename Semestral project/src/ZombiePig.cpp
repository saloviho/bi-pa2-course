#include "ZombiePig.h"

ZombiePig::ZombiePig(int x, int y, SDL_Surface * render) : Monster(TextureManager::zombiePigSurface, render, 3, 0.5, 2, 500, 50, x, y, 60, 140) { }

string ZombiePig::toString()
{
	string result;
	result += "ZP:" + getInfo();
	return result;
}
shared_ptr<Item> ZombiePig::getDrop() { return NULL; }
