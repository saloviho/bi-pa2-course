#ifndef Player_H
#define Player_H

#include <SDL2/SDL.h>
#include <memory>
#include <vector>

#include "Inventory.h"
#include "Movable.h"

using namespace std;

/*!
 * Class represents player
 */
class Player : public Movable
{
public:
	/*!
	 * Constructor
	 * @param x X-Position
	 * @param y Y-Position
	 * @param render Surface to draw
	 */
	Player(int x, int y, SDL_Surface * render);

	/*!
	 * Getter for selected item param
	 * @return Returns selected item index
	 */
	int getSelectedItem();

	/*!
	 * Makes an item selected item
	 * @param n Item index
	 */
	void selectItem(size_t n);

	/*!
	 * Gets selected item
	 * @return Item which is selected by player
	 */
	shared_ptr<Item> getItem();


	/*!
	 * Draws player inventory
	 */
	void drawi();

	/*!
	 * Hits objects
	 * @param collisions Array of objects to be hit
	 */
	void hit(vector<shared_ptr<Object>> collisions);

	/*!
	 * Puts item to inventory
	 * @param item Item to be put
	 */
	void harvest(shared_ptr<Item> item);

	/*!
	 * Places selected item to the map
	 * @param map Map to place
	 * @param x X-Position to be placed on
	 * @param y Y-Position to be placed on
	 */
	void place(shared_ptr<Map> map, int x, int y);


	/*!
	 * Method for getting damage
	 * Adds animation
	 */
	void getDmg(int value);

	/*!
	 * Method draws object on screen
	 * @param time Past time
	 * @param offsetX X-Axis offset
	 * @param offsetY Y-Axis offset
	 * @param map Map to solve collisions
	 */
	void draw(int time, int offsetX, int offsetY, shared_ptr<Map> m);
	
	/*!
	 * Method returns item which drops after block destroying or enemy killing
	 * @return Returns item as reward for killing
	 */
	shared_ptr<Item> getDrop();

	/*!
	 * Creates string representation of a player
	 * @return Returns string representation of a player
	 */
	string toString();

protected:
	Inventory inv;		/*!< Player inventory*/
	size_t selectedItem;	/*!< Selected item*/
};

#endif
