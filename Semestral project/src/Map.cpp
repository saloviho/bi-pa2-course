#include "Map.h"

Map::Map(int lb, int rb) : ground(5), left_border(lb), right_border(rb) { }
void Map::draw(int time, int offsetX, int offsetY)
{
	for (size_t i = 0; i < map.size(); i++)
	{
		for (int j = 0; j < 20; j++)
		{
			if (map[i][j] != NULL)
			{
				if (map[i][j]->isAlive() == false) map[i][j] = NULL;
				else map[i][j]->draw(time, offsetX, offsetY, NULL);
			}
		}
	}
}



void Map::addColumnBefore(vector<shared_ptr<Object>> column)
{
	map.push_front(column);
}

void Map::addColumnAfter(vector<shared_ptr<Object>> column)
{
	map.push_back(column);
}

string Map::toString()
{
	stringstream ss;
	string result;

	ss << left_border;
	string lb = ss.str();
	ss.str(string());
	ss << right_border;
	string rb = ss.str();
	ss.str(string());
	ss << map.size();
	string ms = ss.str();

	result = result + lb + ":" + rb + ":" + ms + "\n";
	for (size_t i = 0; i < map.size(); i++)
	{
		for (int j = 0; j < 20; j++)
		{
			if (map[i][j] == NULL) result = result + "0\n";
			else result = result + map[i][j]->toString() + "\n";
		}
	}

	return result;
}
