#ifndef __PROGTEST__
#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <algorithm>
#include <memory>
#include <functional>
using namespace std;

class CTimeStamp
{
public:
	CTimeStamp(
		int year,
		int month,
		int day,
		int hour,
		int minute,
		double sec
	) : m_year(year), m_month(month), m_day(day), m_hour(hour), m_minute(minute), m_sec(sec) {}

	int Compare(const CTimeStamp & x) const {
		if (m_year < x.m_year) return -1;
		if (m_year > x.m_year) return 1;
		else {
			if (m_month < x.m_month) return -1;
			else if (m_month > x.m_month) return 1;
			else {
				if (m_day < x.m_day) return -1;
				else if (m_day > x.m_day) return 1;
				else {
					if (m_hour < x.m_hour) return -1;
					else if (m_hour > x.m_hour) return 1;
					else {
						if (m_minute < x.m_minute) return -1;
						else if (m_minute > x.m_minute) return 1;
						else {
							if (m_sec < x.m_sec) return -1;
							else if (m_sec > x.m_sec) return 1;
							else return 0;
						}
					}
				}
			}
		}
	}

	friend ostream & operator << (ostream & os, const CTimeStamp & x) {
		os << x.m_year << "-" << setfill('0') << setw(2) << x.m_month << "-" << setfill('0') << setw(2) << x.m_day << " " << setfill('0') << setw(2) << x.m_hour << ":" << setfill('0') << setw(2) << x.m_minute << ":" << setprecision(3) << fixed << setfill('0') << setw(5) << x.m_sec;
		return os;
	}

private:
	int m_year;
	int m_month;
	int m_day;
	int m_hour;
	int m_minute;
	double m_sec;
};
class CMail
{
public:
	CMail(const CTimeStamp & timeStamp,
		const string     & from,
		const string     & to,
		const string     & subject) :m_ts(timeStamp), m_from(from), m_to(to), m_subject(subject) { }

	int CompareByTime(const CTimeStamp & x) const {
		return this->m_ts.Compare(x);
	}

	int CompareByTime(const CMail      & x) const {
		return this->m_ts.Compare(x.TimeStamp());
	}
	const string & From(void) const { return m_from; }
	const string & To(void) const { return m_to; }
	const string & Subject(void) const { return m_subject; }
	const CTimeStamp & TimeStamp(void) const { return m_ts; }
	friend ostream & operator << (ostream & os, const CMail & x) {
		os << x.TimeStamp() << " " << x.From() << " -> " << x.To() << ", " << "subject: " << x.Subject();
		return os;
	}

private:
	CTimeStamp m_ts;
	string m_from;
	string m_to;
	string m_subject;
};
// your code will be compiled in a separate namespace
namespace MysteriousNamespace {
#endif /* __PROGTEST__ */
	//----------------------------------------------------------------------------------------

	struct Comparator
	{
		bool operator() (const CTimeStamp & a, const CTimeStamp & b) const
		{
			if (a.Compare(b) < 0) return true;
			return false;
		}
	};


	class CMailLog
	{
	public:
		int ParseLog(istream          & in)
		{
			char buffer[1000];
			map<string, CMail> tmp;
			int counter = 0;
			while (in.getline(buffer, 999)) {
				string a(buffer);
				parseRow(a, tmp, counter);
			}
			return counter;
		}

		list<CMail> ListMail(const CTimeStamp & from, const CTimeStamp & to) const
		{
			list<CMail> l;
			if (from.Compare(to) > 0) return l;

			auto it1 = data.lower_bound(from);
			auto it2 = data.upper_bound(to);

			if (it1 == data.end()) return l;
			for (; it1 != it2; ++it1) l.insert(l.end(), it1->second.begin(), it1->second.end());
			return l;
		}

		set<string> ActiveUsers(const CTimeStamp & from, const CTimeStamp & to) const
		{
			set<string> l;
			if (from.Compare(to) > 0) return l;

			auto it1 = data.lower_bound(from);
			auto it2 = data.upper_bound(to);

			if (it1 == data.end()) return l;

			for (; it1 != it2; ++it1)
			{
				for (auto it3 = it1->second.begin(); it3 != it1->second.end(); ++it3)
				{
					l.insert(it3->From());
					l.insert(it3->To());
				}
			}

			return l;
		}
	private:

		int days[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		int isV(int year) {
			if (year % 400 == 0) return 1;
			if (year % 100 == 0) return 0;
			if (year % 4 == 0) return 1;
			return 0;
		}

		int VM(string s)
		{
			return mToI(s);
		}

		int VD(string s) {
			if (s.size() == 0) return -1;
			int day = 0;
			try {
				day = stoi(s);
			}
			catch (const invalid_argument& ia) {
				return -1;
			}
			return day;
		}

		bool VD(int d, int month, int year) {
			if (d <= 0 || d > days[month - 1] + (month == 2) ? isV(year) : 0) return false;
			return true;
		}

		int VY(string s) {
			if (s.size() == 0) return -1;
			int year = 0;
			try {
				year = stoi(s);
			}
			catch (const invalid_argument& ia) {
				return -1;
			}
			if (year <= 0) return -1;
			return year;
		}

		int VH(string s) {
			if (s.size() == 0) return -1;
			int hour = 0;
			try {
				hour = stoi(s);
			}
			catch (const invalid_argument& ia) {
				return -1;
			}
			if (hour < 0 || hour >= 24) return -1;
			return hour;
		}

		int VMin(string s) {
			if (s.size() == 0) return -1;
			int minute = 0;
			try {
				minute = stoi(s);
			}
			catch (const invalid_argument& ia) {
				return -1;
			}
			if (minute < 0 || minute >= 60) return -1;
			return minute;
		}

		double VSec(string s) {
			if (s.size() == 0) return -1;
			double second = 0;
			try {
				second = stod(s);
			}
			catch (const invalid_argument& ia) {
				return -1;
			}
			if (second < 0 || second >= 60) return -1;
			return second;
		}

		string VDns(string s) {
			if (s.size() == 0) return "";
			size_t pos = s.find('.');
			if (pos == string::npos) return "";

			if (s[s.size() - 1] == '.' || s[0] == '.') return "";
			return s;
		}

		string VId(string s) {
			if (s.size() == 0) return "";
			size_t i = 0;
			for (; i < s.size() - 1; i++) {
				if ((s[i] >= 'a' && s[i] <= 'z') || (s[i] >= 'A' && s[i] <= 'Z') || (s[i] >= '0' && s[i] <= '9')) {}
				else return "";
			}
			if (s[i] != ':') return "";
			return s;
		}

		string VHead(string s) {
			if (s.size() == 0) return "";
			if (s != "from" && s != "to" && s != "subject") return "";
			return s;
		}

		int mToI(string month) {
			if (month == "Jan") return 1;
			if (month == "Feb") return 2;
			if (month == "Mar") return 3;
			if (month == "Apr") return 4;
			if (month == "May") return 5;
			if (month == "Jun") return 6;
			if (month == "Jul") return 7;
			if (month == "Aug") return 8;
			if (month == "Sep") return 9;
			if (month == "Oct") return 10;
			if (month == "Nov") return 11;
			if (month == "Dec") return 12;
			return -1;
		}

		void parseRow(string & s, map<string, CMail> & tmp, int & counter)
		{
			size_t pos = s.find(' ');
			if (pos == string::npos) return;
			int month = VM(s.substr(0, pos));
			if (month == -1) return;
			s.erase(0, pos + 1);

			pos = s.find(' ');
			if (pos == string::npos) return;
			int day = VD(s.substr(0, pos));
			if (day == -1) return;
			s.erase(0, pos + 1);

			pos = s.find(' ');
			if (pos == string::npos) return;
			int year = VY(s.substr(0, pos));
			if (year == -1) return;
			if (!VD(day, month, year)) return;
			s.erase(0, pos + 1);

			pos = s.find(':');
			if (pos == string::npos) return;
			int hour = VH(s.substr(0, pos));
			if (hour == -1) return;
			s.erase(0, pos + 1);

			pos = s.find(':');
			if (pos == string::npos) return;
			int minute = VMin(s.substr(0, pos));
			if (minute == -1) return;
			s.erase(0, pos + 1);

			pos = s.find(' ');
			if (pos == string::npos) return;
			double second = VSec(s.substr(0, pos));
			if (second == -1) return;
			s.erase(0, pos + 1);

			CTimeStamp timestamp(year, month, day, hour, minute, second);

			pos = s.find(' ');
			if (pos == string::npos) return;
			string server = VDns(s.substr(0, pos));
			if (server == "") return;
			s.erase(0, pos + 1);

			pos = s.find(' ');
			if (pos == string::npos) return;
			string id = VId(s.substr(0, pos));
			if (id == "") return;
			s.erase(0, pos + 1);

			pos = s.find('=');
			if (pos == string::npos) return;
			string header = VHead(s.substr(0, pos));
			if (header == "") return;
			s.erase(0, pos + 1);

			if (header == "to")
			{
				auto it = tmp.find(id);
				if (it == tmp.end()) return;
				CMail a(timestamp, it->second.From(), s, it->second.Subject());

				auto it1 = data.find(timestamp);
				if (it1 == data.end()) {
					list<CMail> l;
					l.push_back(a);
					data.emplace(timestamp, l);
				}
				else it1->second.push_back(a);
				counter = counter + 1;
			}
			else if (header == "from") {
				tmp.emplace(id, CMail(timestamp, s, "", ""));
			}
			else if (header == "subject") {
				auto it = tmp.find(id);
				if (it == tmp.end()) return;
				it->second = CMail(timestamp, it->second.From(), "", s);
			}
		}

		map<CTimeStamp, list<CMail>, Comparator> data;
	};
	//----------------------------------------------------------------------------------------
#ifndef __PROGTEST__
} // namespace
string printMail(const list<CMail> & all)
{
	ostringstream oss;
	for (const auto & mail : all)
		oss << mail << endl;
	return oss.str();
}
string printUsers(const set<string> & all)
{
	ostringstream oss;
	bool first = true;
	for (const auto & name : all)
	{
		if (!first)
			oss << ", ";
		else
			first = false;
		oss << name;
	}
	return oss.str();
}
int main(void)
{
	MysteriousNamespace::CMailLog m;
	list<CMail> mailList;
	set<string> users;
	istringstream iss;

	iss.clear();
	iss.str(
		"Mar 29 2019 12:35:32.233 relay.fit.cvut.cz ADFger72343D: from=user1@fit.cvut.cz\n"
		"Mar 29 2019 12:37:16.234 relay.fit.cvut.cz JlMSRW4232Df: from=person3@fit.cvut.cz\n"
		"Mar 29 2019 12:55:13.023 relay.fit.cvut.cz JlMSRW4232Df: subject=New progtest homework!\n"
		"Mar 29 2019 13:38:45.043 relay.fit.cvut.cz Kbced342sdgA: from=office13@fit.cvut.cz\n"
		"Mar 29 2019 13:36:13.023 relay.fit.cvut.cz JlMSRW4232Df: to=user76@fit.cvut.cz\n"
		"Mar 29 2019 13:55:31.456 relay.fit.cvut.cz KhdfEjkl247D: from=PR-department@fit.cvut.cz\n"
		"Mar 29 2019 14:18:12.654 relay.fit.cvut.cz Kbced342sdgA: to=boss13@fit.cvut.cz\n"
		"Mar 29 2019 14:48:32.563 relay.fit.cvut.cz KhdfEjkl247D: subject=Business partner\n"
		"Mar 29 2019 14:58:32.000 relay.fit.cvut.cz KhdfEjkl247D: to=HR-department@fit.cvut.cz\n"
		"Mar 29 2019 14:25:23.233 relay.fit.cvut.cz ADFger72343D: mail undeliverable\n"
		"Mar 29 2019 15:02:34.231 relay.fit.cvut.cz KhdfEjkl247D: to=CIO@fit.cvut.cz\n"
		"Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=CEO@fit.cvut.cz\n"
		"Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=dean@fit.cvut.cz\n"
		"Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=vice-dean@fit.cvut.cz\n"
		"Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=archive@fit.cvut.cz\n");
	assert(m.ParseLog(iss) == 8);
	mailList = m.ListMail(CTimeStamp(2019, 3, 28, 0, 0, 0),
		CTimeStamp(2019, 3, 29, 23, 59, 59));
	cout << printMail(mailList) << endl;
	assert(printMail(mailList) ==
		"2019-03-29 13:36:13.023 person3@fit.cvut.cz -> user76@fit.cvut.cz, subject: New progtest homework!\n"
		"2019-03-29 14:18:12.654 office13@fit.cvut.cz -> boss13@fit.cvut.cz, subject: \n"
		"2019-03-29 14:58:32.000 PR-department@fit.cvut.cz -> HR-department@fit.cvut.cz, subject: Business partner\n"
		"2019-03-29 15:02:34.230 PR-department@fit.cvut.cz -> CEO@fit.cvut.cz, subject: Business partner\n"
		"2019-03-29 15:02:34.230 PR-department@fit.cvut.cz -> dean@fit.cvut.cz, subject: Business partner\n"
		"2019-03-29 15:02:34.230 PR-department@fit.cvut.cz -> vice-dean@fit.cvut.cz, subject: Business partner\n"
		"2019-03-29 15:02:34.230 PR-department@fit.cvut.cz -> archive@fit.cvut.cz, subject: Business partner\n"
		"2019-03-29 15:02:34.231 PR-department@fit.cvut.cz -> CIO@fit.cvut.cz, subject: Business partner\n");
	mailList = m.ListMail(CTimeStamp(2019, 3, 28, 0, 0, 0),
		CTimeStamp(2019, 3, 29, 14, 58, 32));
	assert(printMail(mailList) ==
		"2019-03-29 13:36:13.023 person3@fit.cvut.cz -> user76@fit.cvut.cz, subject: New progtest homework!\n"
		"2019-03-29 14:18:12.654 office13@fit.cvut.cz -> boss13@fit.cvut.cz, subject: \n"
		"2019-03-29 14:58:32.000 PR-department@fit.cvut.cz -> HR-department@fit.cvut.cz, subject: Business partner\n");
	mailList = m.ListMail(CTimeStamp(2019, 3, 30, 0, 0, 0),
		CTimeStamp(2019, 3, 30, 23, 59, 59));
	assert(printMail(mailList) == "");
	users = m.ActiveUsers(CTimeStamp(2019, 3, 28, 0, 0, 0),
		CTimeStamp(2019, 3, 29, 23, 59, 59));
	assert(printUsers(users) == "CEO@fit.cvut.cz, CIO@fit.cvut.cz, HR-department@fit.cvut.cz, PR-department@fit.cvut.cz, archive@fit.cvut.cz, boss13@fit.cvut.cz, dean@fit.cvut.cz, office13@fit.cvut.cz, person3@fit.cvut.cz, user76@fit.cvut.cz, vice-dean@fit.cvut.cz");
	users = m.ActiveUsers(CTimeStamp(2019, 3, 28, 0, 0, 0),
		CTimeStamp(2019, 3, 29, 13, 59, 59));
	assert(printUsers(users) == "person3@fit.cvut.cz, user76@fit.cvut.cz");


	MysteriousNamespace::CMailLog n;
	list<CMail> mailList1;
	set<string> users1;

	iss.clear();
	iss.str(
		"Mar 29 2019 12:35:32.233 relay. ADFger72343D: from=user1@fit.cvut.cz\n"
		"Mar 29 2019 12:35:32.233 .relay ADFger72343D: from=user1@fit.cvut.cz\n"
		"Mar 2019 12:35:32.233 .relay ADFger72343D: from=user1@fit.cvut.cz\n"
		"Mar 29 2019 12:35:32.233 relay.fit.cvut.cz ADFger72343D: from=user1@fit.cvut.cz\n"
		"Mar 29 2019 12:37:16.234 relay.fit.cvut.cz JlMSRW4232Df: from=person3@fit.cvut.cz\n"
		"Mar 29 2019 12:55:13.023 relay.fit.cvut.cz JlMSRW4232Df: subject=New progtest homework!\n"
		"Mar 29 2019 13:38:45.043 relay.fit.cvut.cz Kbced342sdgA: from=office13@fit.cvut.cz\n"
		"Mar 29 2019 13:36:13.023 relay.fit.cvut.cz JlMSRW4232Df: to=user76@fit.cvut.cz\n"
		"Mar 29 2019 13:55:31.456 relay.fit.cvut.cz KhdfEjkl247D: from=PR-department@fit.cvut.cz\n"
		"Mar 29 2019 14:18:12.654 relay.fit.cvut.cz Kbced342sdgA: to=boss13@fit.cvut.cz\n"
		"Mar 29 2019 14:48:32.563 relay.fit.cvut.cz KhdfEjkl247D: subject=Business partner\n"
		"Mar 29 2019 14:58:32.000 relay.fit.cvut.cz KhdfEjkl247D: to=HR-department@fit.cvut.cz\n"
		"Mar 29 2019 14:25:23.233 relay.fit.cvut.cz ADFger72343D: mail undeliverable\n"
		"Mar 29 2019 14:58:32.000 relay.fit.cvut.cz KhdfEjkl247D: to=CIO@fit.cvut.cz\n"
		"Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=CEO@fit.cvut.cz\n"
		"Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=dean@fit.cvut.cz\n"
		"Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=vice-dean@fit.cvut.cz\n"
		"Mar 29 2019 15:02:34.230 relay.fit.cvut.cz KhdfEjkl247D: to=archive@fit.cvut.cz\n");
	assert(n.ParseLog(iss) == 8);
	mailList1 = n.ListMail(CTimeStamp(2099, 3, 28, 0, 0, 0),
		CTimeStamp(2019, 3, 29, 23, 59, 59));
	cout << printMail(mailList1) << endl;
	return 0;
}
#endif /* __PROGTEST__ */
